shopt -s globstar
shopt -s nullglob

#cd "/media/dave/TOSHIBA EXT/V Logs"

found=( **.wmv )

for f in "${found[@]}"; do
	year=20"${f::2}"
	mo="${f:2:2}"
	day="${f:4:2}"
	exiftool "-AllDates=$year:$mo:$day 12:00:00" "$f"
	#touch -t "${f::6}"0000 "$f"
done